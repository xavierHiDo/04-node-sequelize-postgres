const User = require('../models/tutorial.model');

exports.getUsers = async (req, res) => {
  try {
    const users = await User.findAll();
    return res.status(200).json({users});
  } catch (error) {
    return res.status(500).json({error});
  }
}

exports.getUser = async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id);
    return res.status(200).json({user});
  } catch (error) {
    return res.status(500).json({error});
  }
}

exports.postUser = async (req, res) => {
  try {
    const USER_MODEL = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
    }
    try {
      const user = await User.create(USER_MODEL);
      return res.status(201).json({user});
    } catch (error) {
      return res.status(500).json({error});
    }
  } catch (error) {
    return res.status(500).json({error});
  }
}

exports.putUser = async (req, res) => {
  try {
    const USER_MODEL = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
    }
    try {
      const user = await User.update(USER_MODEL, {where: {id: req.params.id}});
      return res.status(201).json({user});
    } catch (error) {
      return res.status(500).json({error});
    }
  } catch (error) {
    return res.status(500).json({error});
  }
}

exports.deleteUser = async (req, res) => {
  try {
    const user = await User.destroy({where: {id: req.params.id}});
    return res.status(200).json({user});
  } catch (error) {
    return res.status(500).json({error});
  }
}