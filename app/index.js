const express = require("express");
require('dotenv').config();

const sequelize = require('./utils/database');
const User = require('./models/tutorial.model');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET', 'POST', 'PUT', 'DELETE');
  next();
});

app.get("/", (req, res) => {
  res.json({ message: 'Welcome to this API.' });
});

app.use('/api', require('./routes/tutorial.route'));
const PORT = process.env.PORT || 8080;


(async () => {
  try {
    await sequelize.sync({ force: true });
    app.listen(PORT, () => {
      console.log('Server running on port: ', PORT);
    });
  } catch (error) {
    console.error(error);
  }
})()