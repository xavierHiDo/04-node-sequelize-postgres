const controller  = require('../controllers/tutorial.controller');
const router = require("express").Router();

router
  .get('/', controller.getUsers)
  .get('/:id', controller.getUser)
  .post('/', controller.postUser)
  .put('/:id', controller.putUser)
  .delete('/:id', controller.deleteUser);

module.exports = router;